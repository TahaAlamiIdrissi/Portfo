const hambOne = document.getElementById("hamb-one");
const hambTwo = document.getElementById("hamb-two");
const hambThree = document.getElementById("hamb-three");
const name = document.getElementById("name");
const email = document.getElementById("email");
const message = document.getElementById("message");
const right = document.getElementById("right-panel");
const ruy = document.getElementById("ruy");

let flagHadou = false;

ruy.addEventListener("click", e => {
  if (!flagHadou) {
    ruy.setAttribute("src", "./img/hadouken.png");
    right.style.background = "rgb(2, 26, 129)";
    ruy.style.filter = "drop-shadow(5px 10px 10px blue)";
    flagHadou = true;
  } else {
    ruy.setAttribute("src", "./img/ruy.png");
    right.style.background = "rgba(0, 0, 0, 0.3)";
    flagHadou = false;
  }
});

let flag = false;
$("#nav-items").hide();
$("#cards").hide();
$("#skills").hide();
$("#contact").hide();
document.getElementById("hamburger").addEventListener("click", e => {
  if (!flag) {
    flag = true;
    hambOne.setAttribute("class", "hamb-one-close");
    hambTwo.setAttribute("class", "hamb-two-close");
    hambThree.setAttribute("class", "hamb-three-close");
    $("#nav-items").show();
  } else {
    flag = false;
    hambOne.setAttribute("class", "hamb-one");
    hambTwo.setAttribute("class", "hamb-two");
    hambThree.setAttribute("class", "hamb-three");
    $("#nav-items").hide();
  }
});

$("#projects").click(e => {
  e.preventDefault();
  if (!$("#cards").is(":visible")) {
    $("#cards").show();
    $("#skills").hide();
    $("#contact").hide();
  } else {
    $("#cards").hide();
  }
});

$("#slliks").click(e => {
  e.preventDefault();

  if (!$("#skills").is(":visible")) {
    $("#skills").show();
    $("#cards").hide();
    $("#contact").hide();
  } else {
    $("#skills").hide();
  }
});

$("#letstalk").click(e => {
  e.preventDefault();

  if (!$("#contact").is(":visible")) {
    $("#contact").show();
    $("#cards").hide();
    $("#skills").hide();
  } else {
    $("#contact").hide();
  }
});
