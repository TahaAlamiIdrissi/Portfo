/**
 * Sample JavaScript code for youtube.videos.list
 * See instructions for running APIs Explorer code samples locally:
 * https://developers.google.com/explorer-help/guides/code_samples#javascript
 */

const API_KEY = "AIzaSyCIzJN-cwqY5kIbM3T5fwa5VwVkbW8NBoo";
const PLAYLIST_ID = "4fm3BP9Yf2c&list=PL-f06f48HDwuAL58PhLQ0gcap0tWq2g8r";
const URL = "https://www.googleapis.com/youtube/v3/playlistItems";

let options = {
  part: "snippet",
  key: API_KEY,
  maxResults: 20,
  playlistId: PLAYLIST_ID
};

function authenticate() {
  return gapi.auth2
    .getAuthInstance()
    .signIn({ scope: "https://www.googleapis.com/auth/youtube.readonly" })
    .then(
      function() {
        console.log("Sign-in successful");
      },
      function(err) {
        console.error("Error signing in", err);
      }
    );
}


function loadClient() {
  gapi.client.setApiKey(options.key);
  return gapi.client
    .load("https://www.googleapis.com/discovery/v1/apis/youtube/v3/rest")
    .then(
      function() {
        console.log("GAPI client loaded for API");
      },
      function(err) {
        console.error("Error loading GAPI client for API", err);
      }
    );
}
// Make sure the client is loaded and sign-in is complete before calling this method.
function execute() {
  return gapi.client.youtube.videos
    .list({
      part: "snippet,contentDetails,statistics",
      chart: "mostPopular",
      regionCode: "US"
    })
    .then(
      function(response) {
        // Handle the results here (response.result has the parsed body).
        console.log("Response", response);
      },
      function(err) {
        console.error("Execute error", err);
      }
    );
}
gapi.load("client:auth2", function() {
  gapi.auth2.init({ client_id: "YOUR_CLIENT_ID" });
});
